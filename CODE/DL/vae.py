#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""VAE module

File containing VAE Aggregated/Binned Hawkes Process estimation (Generative model).

"""